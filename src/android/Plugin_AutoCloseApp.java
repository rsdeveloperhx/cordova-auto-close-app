package ch.migros.plugin;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;
import java.util.concurrent.ExecutorService;
import org.apache.cordova.*;
import org.json.JSONArray;
import org.json.JSONException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileWriter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.PackageManager;

// RS HXIN
@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class Plugin_AutoCloseApp extends CordovaPlugin {
    private final static String LOGTAG = Plugin_AutoCloseApp.class.getName();
    private final static String AUTO_CLOSE_LOG_FILE = "/sdcard/migrolino_logs/migrolinosd_log.csv";

	private static CordovaInterface s_cordova=null;
	private static AutoCloseWorker  s_worker=new AutoCloseWorker();
	private static String s_appPackage=null;
	private static final DateFormat s_df=new SimpleDateFormat("yyyyMMdd-HHmmss");	
	private static final long MB=1024*1024;
	
    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
        s_cordova=cordova;
		log("initialize AutoCloseApp Plugin");
		init();
		shrinkFile(getLogFile(), 10*MB, 8*MB, true);
    }
	
	private static String getAppName(){
		String appName="n/a";
		try {
			PackageManager packageManager = s_cordova.getActivity().getPackageManager();
			ApplicationInfo app = packageManager.getApplicationInfo(s_cordova.getActivity().getPackageName(), 0);
			appName=(String)packageManager.getApplicationLabel(app);
		} catch (Exception silent){}
		return appName;
		
	}
	
	private static String getAppPackageName() {
		String pkgName="n/a";
		try {
			pkgName=s_cordova.getActivity().getPackageName();
		} catch (Exception silent){}
		return pkgName;
	}

	private static String getAppVersion() {
		String version="n/a";
		try {
			PackageManager packageManager = s_cordova.getActivity().getPackageManager();
			version=packageManager.getPackageInfo(s_cordova.getActivity().getPackageName(), 0).versionName;
		} catch (Exception silent){}
		return version;
	}
	
	private static void log(String txt) {
		String logText=getAppName() + ";" +getAppVersion() 
		      +";"
		      + txt;
		logToFile(logText);
		Log.i(LOGTAG, logText);
	}
	
	@Override
    public boolean execute(final String action, final JSONArray data, final CallbackContext callbackContext) throws JSONException {
		Log.d(LOGTAG, "execute: action="+action +" data:"+String.valueOf(data));
		if (action.equalsIgnoreCase("fileLog")) {
			String logText="";
			try {
				logText = data.getString(0);
				log("fileLog-js: "+logText);
			} catch (Exception silent) {
				log("fileLog-js: failed to parse the log text from passed value.");
			}

		} else if (action.equalsIgnoreCase("startAutoCloseApp")) {
			String time="";
			log("startAutoCloseApp");
			if (data.length()<1) {
				log("startAutoCloseApp: Execution time not passed. Please check the JavaScript Cordova call. The passed time must be formatted as a 'HH:mm' string.");
			}
			try {
				time = data.getString(0);
			} catch (Exception silent) {}
			startAutoCloseApp(time);
		} else if (action.equalsIgnoreCase("stopAutoCloseApp")) {
			log("stopAutoCloseApp");
			stopAutoCloseApp();
		}
		callbackContext.success();
		return true;
	}
	
	private void stopAutoCloseApp() {
		s_worker.stop();
	}
	
	private void startAutoCloseApp(String time) {
		s_worker.setCloseTime(time);
		if (!s_worker.isRunning()) {
			log("start new thread for runnable AutoCloseWorker");
			s_cordova.getThreadPool().execute(s_worker);
		}	
	}
	
    private static int[] parseTime(String time) {
        int h=-1;
        int m=-1;
        try {
            String t[]=time.split(":");
            h=Integer.parseInt(t[0]);
            m=Integer.parseInt(t[1]);
            if (h <0 || h> 23)
                throw new RuntimeException("hour is out of value range. "+ h);
            if (m <0 || m> 59)
                throw new RuntimeException("minute is out of value range. "+m);

        } catch (Exception ex) {
            log("Parse exception: "+ex);
			throw new RuntimeException(ex);
        }
        int [] ret={ h,m };
        return ret;
    }

	
	private static class AutoCloseWorker implements Runnable {
		private static DateFormat s_df_time=new SimpleDateFormat("HH:mm");
		boolean _running=false;
		Date    _nextAutoCloseTime=null;
		
		public void setCloseTime(String autoCloseTime) {
			Calendar cal=new GregorianCalendar();
			Date now=new Date();
			Date schedule=null;
			try {
				String currTime=s_df_time.format(now);
				int[] ct=parseTime(currTime);
				int[] at=parseTime(autoCloseTime);

				cal.set(Calendar.HOUR_OF_DAY,at[0]);
				cal.set(Calendar.MINUTE,at[1]);
				schedule=cal.getTime();
				if (now.compareTo(schedule)>0) { //wenn der Auto Close Zeitpunkt schon vorüber ist, dann auf nächsten Tag zur selben Zeit einplanen
					cal.add(Calendar.DAY_OF_YEAR,1);
					schedule=cal.getTime();
				}
				_nextAutoCloseTime=schedule;
				log("Passed time is '"+ autoCloseTime +"'. Schedule next app auto close at '"+_nextAutoCloseTime +"'");
			} catch (Exception ex) {
				log("Passed time '"+ autoCloseTime +"' cannot be parsed. Use default value instead. Set to 03:30");
				setCloseTime("03:30");
			}
		}
		
		public boolean isRunning() {
			return _running;
		}
		
		public void stop() {
			_running=false;
		}
	
		public void run() {
			boolean _running=true;
			log("run periodic app auto close thread.");
			do {
				checkTimeToClose();
				sleep (60000);
			} while (_running);
			log("stop periodic app auto close thread.");
		}

		private void checkTimeToClose() {
			Date now=new Date();
			
			/* nur für Debugging
			log("checkTimeToClose() currTime:"+now.toString() 
			    + " nextAutoCloseTime: " +_nextAutoCloseTime);
			*/
			
			if (_nextAutoCloseTime.compareTo(new Date())<0) {
				s_cordova.getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						
						log("checkTimeToClose() auto close app now. currTime:"+now.toString() 
							+ " nextAutoCloseTime: " +_nextAutoCloseTime);

						showAutoCloseInfo();
					}
				});
				sleep(10000);
				System.exit(0);
			}
		}

		private static void sleep(int mills) {
			try {
				Thread.sleep(mills);
			} catch (Exception ex){}
		}
		
		private void showAutoCloseInfo() {
			Toast.makeText(s_cordova.getActivity(),"App automatic close scheduled time reached. This app will be closed in 10 seconds." + _nextAutoCloseTime,Toast.LENGTH_LONG).show();
		}
		
	};

	public static void init() {
		if (s_appPackage==null) {
            String s=s_cordova.getActivity().getClass().toString();
            s_appPackage = s_cordova.getActivity().getClass().getPackage().getName();
        }
	}

    public static void logToFile(String logText) {
        File f=getLogFile();
        FileWriter fw=null;
        try {
            fw=new FileWriter(f,true);                       //Example
            fw.write(s_df.format(System.currentTimeMillis()) //20200131-140125
                    +";"+s_appPackage                        //ch.migrolino.neptune.dev;MigrolinoD;1.5.4
					+";"+logText                             //passed log text
					+"\r\n");
            fw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {fw.close();} catch (Exception ex){};
            setFileAccess4All(f);
        }
    }
	
    private static File getLogFile() {
        File f=new File(AUTO_CLOSE_LOG_FILE);
        ensureFolderExists(f.getParent());      //MP FM Make sure directory exist !
        setFileAccess4All(f);
		return f;
    }
	
	/* ----------------------------
		Kopiert von FileUtil Klasse 
	   ----------------------------
	*/
	public static boolean ensureFolderExists(String folder) {
        File f=new File(folder);
        return ensureFolderExists(f);
    }

    public static boolean ensureFolderExists(File folder) {
        try {
            folder.mkdirs();
            setFileAccess4All(folder);
            return true;
        } catch (Exception silent) {System.out.println(silent);}
        return false;
    }

    /**
     * Alle Dateien unter /enterprise/usr werden durch das Gerät automatisch
     * nach jedem Neustart mit den Rechten 777 ausgestattet. Um den Zugriff
     * auch ohne neustart zu gewährleisten werden diese Rechte auch aus diesem
     * Programm beim erstellen einer Datei oder Ordner eingeräumt.
     * @param f
     */
    public static void setFileAccess4All(File f) {
        if (f!=null) {
            f.setReadable(true, false);
            f.setWritable(true, false);
            f.setExecutable(true, false);
        }
    }
	
	   /**
     * Begrenzt die maximale Dateilänge.
     * Die Datei wird auf die übergebene Anzahl Bytes reduziert
     */
    public static boolean shrinkFile(File f, long maxSize, long shrinkSize, boolean shrinkAtNewLine) {
        long size=f.length();
        if (size< maxSize)
            return false;

        long pos=size-shrinkSize+1;
        if (pos<1)
            return false;

        InputStream is=null;
        OutputStream os=null;
        File ft=null;
        try {
            ft=new File(f.getCanonicalFile()+".tmp");
            is=new FileInputStream(f);
            os=new FileOutputStream(ft);
            is.skip(pos-1);
            int c=0;
            byte[] buf=new byte[4096];
            if (shrinkAtNewLine) {
                while (is.available() > 0) { //sucht den nächsten New Line
                    c = is.read();
                    if (c == '\n')
                        break;
                }
            }
            int len=-1;
            while(is.available()>0) {
                len=is.read(buf);
                os.write(buf, 0, len);
            }
        } catch (Exception e) {
            Log.e(LOGTAG,"Shrink logfile failed. exception was"+e.toString(),e);
        } finally {
            try {is.close();} catch (IOException silent) {}
            try {os.close();} catch (IOException silent) {}

            if (ft.exists()) {
                try {f.delete();} catch (Exception silent) {}
                try {ft.renameTo(f);} catch (Exception silent) {}
            }
        }
        return true;
    }
	

	
}
